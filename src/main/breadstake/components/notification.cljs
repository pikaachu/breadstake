(ns breadstake.components.notification
  (:require ["react-dom" :refer (createPortal)]
            ["react" :refer [Fragment useState]]
            ["@headless-ui/react" :refer (Transition)]
            ["@heroicons/react/solid" :refer (XIcon)]
            [reagent.core :as r]))

(defn notification-wrapper [children]
  (let [el (.. js/document (getElementById "notifications"))]
    (createPortal (r/as-element children) el)))

(defn notification [attrs & children]
  (let [show? (r/atom true)]
    (fn [attrs & children]
      [:> Transition {:show @show?
                      :as Fragment
                      :enter "transform ease-out duration-300 transition"
                      :enter-from "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
                      :enter-to "translate-y-0 opacity-100 sm:translate-x-0"
                      :leave "transition ease-in duration-100"
                      :leave-from "opacity-100"
                      :leave-to "opacity-0"}
       [:div {:class "max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden"}
        [:div.p-4
         [:div.flex.items-start

          ; icon

          ; body
          [:div {:class "ml-3 w-0 flex-1 pt-0.5"}
           children]

          ; close button
          [:div.ml-4.flex-shrink-0.flex
           [:button {:class "bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                     :on-click #(reset! show? false)}
            [:span.sr-only "Close"]
            [:> XIcon {:class "h-5 w-5" :aria-hidden true}]]]]]]])))
