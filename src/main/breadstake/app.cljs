(ns breadstake.app
  (:require [reagent.dom :as dom]
            [re-frame.core :as r]
            [breadstake.views :as views]
            [breadstake.events :as events]
            [breadstake.db :as db]))

(defn app
  []
  [:div
   [:div.py-10.space-y-10
    [:header
     [:div {:class "max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"}
      [:h1 {:class "text-3xl font-bold leading-tight text-gray-900"}
       "Breadstake"]]]
    [:main
     [views/staking-card]]]
   [views/spending-notification]])

;; start is called by init and after code reloading finishes
(defn ^:dev/after-load start []
  (r/dispatch-sync [:boot])

  (dom/render [app]
    (.getElementById js/document "app")))

(defn init []
  ;; init is called ONCE when the page loads
  ;; this is called in the index.html and must be exported
  ;; so it is available even in :advanced release builds
  (js/console.log "init")
  (js/window.ethereum.enable)
  (start))

;; this is called before any code is reloaded
(defn ^:dev/before-load stop []
  (js/console.log "stop"))
