(ns breadstake.views
  (:require [breadstake.events :as events]
            [breadstake.subs :as subs]
            [breadstake.db :as db]
            [breadstake.utils :refer [parse-units]]
            [re-frame.core :refer [subscribe]]
            [re-frame.core :as r]
            ["@headlessui/react" :refer (Dialog Transition)]
            ["react" :refer (Fragment)]
            ["@heroicons/react/outline" :refer (CheckCircleIcon)]
            ["@heroicons/react/solid" :refer (XIcon)]))

(def DialogOverlay (.-Overlay Dialog))
(def DialogTitle (.-Title Dialog))
(def DialogDescription (.-Description Dialog))

(defn from-badge [children]
  [:div
   [:span {:class ["badge" "bg-red-100" "text-red-800"]}
    children]])

(defn to-badge [children]
  [:div
   [:span {:class ["badge" "bg-blue-100" "text-blue-800"]}
    children]])

(defn spending-notification []
  [:div
   {:aria-live "assertive"
    :class "fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start"}
   [:div
    {:class "w-full flex flex-col items-center space-y-4 sm:items-end"}
    [:> Transition
     {:show @(subscribe [:processing-tx?])
      :enter "transform ease-out duration-300 transition"
      :enterFrom "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
      :enterTo "translate-y-0 opacity-100 sm:translate-x-0"
      :leave "transition ease-in duration-100"
      :leaveFrom "opacity-100"
      :leaveTo "opacity-0"
      :as Fragment}
     [:div
      {:class
       "max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden"}
      [:div {:class "p-4"}
       [:div {:class "flex items-start"}
        [:div {:class "flex-shrink-0"}
         [:> CheckCircleIcon
          {:className "h-6 w-6 text-green-400"
           :aria-hidden "true"}]]
        [:div {:class "ml-3 w-0 flex-1 pt-0.5"}
         [:p {:class "text-sm font-medium text-gray-900"}
          "Transaction submitted!"]]]]]]]])

(defn card [children]
  [:div {:class "bg-white overflow-hidden shadow sm:rounded-lg"}
   [:div {:class "px-4 py-5 sm:p-6"}
    children]])

(defn submit-button []
 [:button
  {:type "button"
   :on-click #(r/dispatch [:init-exchange])
   :class
    "inline-flex justify-center items-center px-6 py-3 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"}
 "Transfer"]
 )

(defn trailing-input [{:keys [currency on-change on-blur value]}]
  [:div.mt-1.relative.rounded-md.shadow-sm
   [:input {:type "text"
            :name "price"
            :id "price"
            :class "focus:ring-red-500 focus:border-red-500 block w-full pl-7 pr-12 sm:text-sm border-gray-300 rounded-md"
            :on-change on-change
            :on-blur on-blur
            :value value
            :placeholder "0.00"}]
   [:div {:class "absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none"}
    [:span {:class "text-gray-500 sm:text-sm"}
     currency]]])

(defn balance-stats [balance currency]
  [:div.flex.flex-row.justify-between.pt-1.text-gray-600.text-sm
   [:div "Available balance: " [:span.font-bold balance " " currency]
    [:div.flex.flex-row.justify-end ]]])

(defn to-balance-stats []
  [:div.flex.flex-row.justify-between.pt-1.text-gray-600.text-sm
   [:div
    [:div.flex.flex-row.justify-end  "Available balance: " @(subscribe [:to-balance]) " " @(subscribe [:to-currency])]]])

(defn from-balance-section []
  [:div.flex.flex-col
   [from-badge "FROM"]
   [:h2.text-xl.font-bold @(subscribe [:from-currency])]
   [trailing-input {:currency @(subscribe [:from-currency])
                    :on-change #(r/dispatch [:set-exchange-amount (-> % .-target .-value)])
                    :on-blur identity
                    :value @(subscribe [:exchange-amount])}]
   [balance-stats @(subscribe [:from-balance]) @(subscribe [:from-currency])]])

(defn to-stake-section [children]
  [:div.flex.flex-col
   [to-badge "TO"]
   [:h2.text-xl.font-bold @(subscribe [:to-currency])]
   [balance-stats @(subscribe [:to-balance]) @(subscribe [:to-currency])]])

(defn switch-button []
  [:button
   {:type "button"
    :on-click #(r/dispatch [:switch-source])
    :class
    "inline-flex items-center p-3 border border-transparent rounded-full shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"}
   [:svg.h-6.w-6 {:xmlns "http://www.w3.org/2000/svg"
                  :fill "none"
                  :viewBox "0 0 24 24"
                  :stroke "currentColor"}
    [:path {:stroke-linecap "round"
            :stroke-linejoin "round"
            :stroke-width "2"
            :d "M7 16V4m0 0L3 8m4-4l4 4m6 0v12m0 0l4-4m-4 4l-4-4"}]]])

(defn staking-card
  []
  [:div {:class "max-w-7xl mx-auto sm:px-6 lg:px-8"}
   [:div {:class "flex flex-col"}
    [card [from-balance-section]]
    [:div {:class "-mt-4 self-center z-10"}
     [switch-button]]
    [:div {:class "-mt-4"}
     [card [to-stake-section]]]
    [:div {:class "flex flex-col mt-4"}
     [submit-button]]]])
