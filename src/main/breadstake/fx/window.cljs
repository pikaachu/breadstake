(ns breadstake.fx.window
  (:require [applied-science.js-interop :as j]
            [day8.re-frame.tracing :refer [fn-traced]]
            [re-frame.core :refer [reg-fx] :as r]))

(reg-fx
 :window
 (fn-traced
  [{:keys [method args on-success on-error]}]
  (-> (j/apply-in js/window method (clj->js args))
      (.then  #(r/dispatch (conj on-success %)))
      (.catch #(r/dispatch (conj on-error %))))))
