(ns breadstake.fx.ethers
  (:require ["ethers" :refer (ethers)]
            [applied-science.js-interop :as j]
            [day8.re-frame.tracing :refer [fn-traced]]
            [re-frame.core :refer [reg-fx] :as r]))

(def Contract (.-Contract ethers))

(reg-fx
 :ethers
 (fn-traced [{:keys [provider method args on-success on-error await?]}]
   (-> (j/apply-in provider method (clj->js args))
       (.then   (fn [resp] (if await? (.wait resp) resp)))
       (.then  #(r/dispatch (conj on-success %)))
       (.catch #(r/dispatch (conj on-error %))))))

(reg-fx
 :ethers/contract
 (fn-traced [{:keys [contract/address
                     contract/abi
                     provider
                     method
                     args
                     on-success
                     on-error
                     await?]}]
   (-> (Contract. address abi provider)
       (j/apply-in method (clj->js args))
       (.then   (fn [resp] (if await? (.wait resp) resp)))
       (.then  #(r/dispatch (conj on-success %)))
       (.catch #(r/dispatch (conj on-error %))))))

