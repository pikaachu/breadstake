(ns breadstake.subs
  (:require [re-frame.core :refer [reg-sub]]
            [breadstake.utils :refer [format-currency]]))


(defn format-balance [balance currency]
  (case (get-in balance [currency :status])
      :error "Error fetching balance"
      :success (format-currency (get-in balance [currency :amount]))
      :fetching "Fetching balance..."))

(reg-sub
 :balances
 (fn [db _]
   (:balance db)))

(reg-sub
 :currency
 (fn [db _] (:from db)))

(reg-sub
 :exchange-amount
 (fn [db _]
   (get-in db [:exchange :input])))

(reg-sub :from-currency (fn [db _] (:from db)))
(reg-sub :to-currency (fn [db _] (:to db)))

(reg-sub
 :processing-tx?
 (fn [db _]
   (:processing-tx? db)))

(reg-sub
 :from-balance

 :<- [:balances]
 :<- [:from-currency]

 (fn [[balance from-currency] _]
   (format-balance balance from-currency)))

(reg-sub
 :to-balance

 :<- [:balances]
 :<- [:to-currency]

 (fn [[balance to-currency] _]
   (format-balance balance to-currency)))
