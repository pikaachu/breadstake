(ns breadstake.stake.events
  (:require [breadstake.fx.ethers]
            [breadstake.fx.window]
            [day8.re-frame.async-flow-fx]
            [breadstake.utils :refer [assoc-in-some parse-units]]
            [day8.re-frame.tracing :refer-macros [defn-traced fn-traced]]
            [re-frame.core :refer [reg-event-fx reg-event-db] :as rf]
            ["ethers" :refer (ethers)]))

(defn- parse-amount [s]
  (try
    (parse-units s "ether")
    (catch js/Error e
      nil)))

(def bread-abi
  #js["function name() public view returns(string)"
      "function symbol() public view returns(string)"
      "function balanceOf(address owner) view returns (uint256)"
      "function mint(uint256 _amount) public"
      "function burn(uint256 _amount) public"
      "event Transfer(address indexed src, address indexed dst, uint val)"])

(def bread-address
  "0x115dA0Cc5Ffad0C0014963bd1fb3DF4cc8c36220")

(def dai-abi
  #js["function approve(address,uint256) external"
      "function balanceOf(address owner) view returns (uint256)"
      "event Transfer(address indexed src, address indexed dst, uint val)"])

(def dai-address
  "0x5592ec0cfb4dbc12d3ab100b257153436a1f0fea")

(def contracts
  {:bread {:abi bread-abi
           :address bread-address
           :success :balance.bread/success
           :error :balance.bread/error}
   :dai {:abi dai-abi
         :address dai-address
         :success :balance.dai/success
         :error :balance.dai/error}})

(defn-traced fetch-accounts
  [{:keys [db]} _]
  {:fx [[:window {:method [:ethereum :request]
                  :args [{:method "eth_accounts"}]
                  :on-success [:accounts/success]
                  :on-error [:accounts/error]}]]})

(defn-traced fetch-address
  [{:keys [db]} _]
  {:fx [[:ethers {:provider (.getSigner (:provider db))
                  :method [:getAddress]
                  :args []
                  :on-success [:address/success]
                  :on-error [:address/error]}]]})

(defn-traced fetch-balance
  [{:keys [db]} [_ currency]]
  (let [{:keys [abi address success error]} (get-in contracts [currency])
        hex-address (get-in db [:address :hex])]
    {:fx [[:ethers/contract {:contract/address address
                             :contract/abi abi
                             :provider (:provider db)
                             :method [:balanceOf]
                             :args [hex-address]
                             :on-success [success]
                             :on-error [error]}]]}))

(defn-traced approve-all
  [{:keys [db]} [_]]
  {:fx [[:ethers/contract {:contract/address dai-address
                           :contract/abi dai-abi
                           :provider (.getSigner (:provider db))
                           :method [:approve]
                           :args [bread-address (.. ethers -constants -MaxUint256)]
                           :on-success [:approved]
                           :on-error [:rejected]}]]})

(defn-traced exchange
  [{:keys [db]} [_ amount]]
  (let [method (case (:from db) "DAI" :mint "BREAD" :burn)]
    {:fx [[:ethers/contract {:contract/address bread-address
                             :contract/abi bread-abi
                             :provider (.getSigner (:provider db))
                             :method [method]
                             :args [amount {:gasLimit 500000}]
                             :await? true
                             :on-success [:exchange-approved]
                             :on-error [:exchange-rejected]}]
          [:dispatch [:show-notification]]]}))

(reg-event-db
 :balance.dai/success
 [(rf/path [:balance "DAI"])]
 (fn-traced
  [_ [_ balance]]
  {:status :success :amount balance}))

(reg-event-db
 :store-response
 (fn-traced
  [db [_ path value]]
  (assoc db path value)))

(reg-event-db
 :balance.dai/error
 [(rf/path [:balance "DAI"])]
 (fn-traced
  [db [_ err]]
  {:status :error :error err}))

(reg-event-db
 :approved
 (fn-traced
  [db event]
  (assoc db :spending-approved? true)))

(reg-event-db
 :rejected
 (fn-traced
  [db [_ err]]
  (-> db
      (assoc :spending-approved? false)
      (assoc :spending-approval-error err))))

(reg-event-db
 :balance.bread/success
 [(rf/path [:balance "BREAD"])]
 (fn-traced
  [db [_ balance]]
  {:status :success :amount balance}))

(reg-event-db
 :balance.bread/error
 [(rf/path [:balance "BREAD"])]
 (fn-traced
  [db [_ err]]
  {:status :error :error err}))

(reg-event-db
 :set-exchange-amount
 (fn-traced
  [db [_ amount]]
  (-> db
      (assoc-in [:exchange :input] amount)
      (assoc-in-some [:exchange :amount] (parse-amount amount)))))

(reg-event-db
 :switch-source
 (fn-traced
  [{:keys [from to] :as db} _]
  (merge db {:to from :from to})))

(reg-event-fx
 :init-exchange
 (fn-traced
  [{:keys [db]} _]
  (let [amount (get-in db [:exchange :amount])]
    {:fx [[:dispatch [:exchange amount]]]})))

(reg-event-fx
 :exchange-approved
 (fn-traced
  [{:keys [db]} _]
  {:fx [[:dispatch [:hide-notification]]
        [:dispatch [:fetch/balance :dai]]
        [:dispatch [:fetch/balance :bread]]] }))

(reg-event-db
 :exchange-rejected
 (fn-traced
  [db [_ err]]
  (assoc db :exchange-rejected err)))

(reg-event-db
 :show-notification
 (fn-traced
  [db _]
  (assoc db :processing-tx? true)))

(reg-event-db
 :hide-notification
 (fn-traced
  [db _]
  (assoc db :processing-tx? false)))

(reg-event-fx :approve-all approve-all)
(reg-event-fx :exchange exchange)
(reg-event-fx :fetch/address fetch-address)
(reg-event-fx :fetch/balance fetch-balance)

(reg-event-fx
 :address/success
 (fn-traced
  [{:keys [db]} [_ address]]
  {:db (assoc db :address {:status :success :hex address})
   :fx [[:dispatch [:fetch/balance :dai]]
        [:dispatch [:fetch/balance :bread]]
        [:dispatch [:approve-all]]]}))

(reg-event-db
 :address/error
 [(rf/path :address)]
 (fn-traced
  [_ [_ err]]
  {:status :error :error err}))
