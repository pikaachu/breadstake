(ns breadstake.events
  (:require [breadstake.db :as db]
            [breadstake.fx.ethers]
            [breadstake.stake.events :as stake-events]
            [day8.re-frame.async-flow-fx :as async-flow-fx]
            [day8.re-frame.tracing :refer [defn-traced]]
            [re-frame.core :refer [reg-event-fx]]
            ["ethers" :refer (ethers)]))

(defn metamask-provider []
  (when js/window.ethereum
    (new (.. ethers -providers -Web3Provider) js/window.ethereum)))

(defn-traced boot
  [cofx]
  {:db  {:provider (metamask-provider)
         :balance {"DAI" {:status :fetching}
                   "BREAD" {:status :fetching}}
         :exchange {:amount 0.00}
         :address {:status :fetching}
         :processing-tx? false
         :from "DAI"
         :to "BREAD"}
   :fx  [[:dispatch [:fetch/address]]]})

(reg-event-fx :boot boot)
