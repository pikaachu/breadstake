(ns breadstake.utils
  (:require [applied-science.js-interop :as j]
            ["ethers" :refer (ethers)]))

(defn assoc-in-some [db k v]
  (let [old-v (get-in db k)]
    (assoc-in db k (or v old-v))))

(defn format-currency [amount]
  (j/call-in ethers [:utils :formatEther] amount))

(defn parse-units [amount decimal-places]
  (j/call-in ethers [:utils :parseUnits] amount decimal-places))
