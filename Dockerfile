FROM theasp/clojurescript-nodejs:shadow-cljs-alpine
WORKDIR /app
COPY package.json package-lock.json shadow-cljs.edn deps.edn /app/
RUN npm install --save-dev shadow-cljs
COPY ./ /app
RUN npm run release
